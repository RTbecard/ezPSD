ezPSD: Streamlined Acoustic Analysis for Biologists
---


> Note:  This package is going through some overhauls at the moment.  All the functions still work as expected, but I'm (slowly) rewriting the vingettes.

### Overview

`ezPSD` was designed as a collection of convenience functions for acoustic analysis in R.
While it started as scripts and wrappers for functions in other packages, I've slowly been replacing these wrappers with home grown functions.
The end goal of this package is to have `ggplot2` as the sole dependancy, after which I'm aiming to publish this on the CRAN.

To get an idea of what `ezPSD` can do, we suggest checking the following documentation:
- [Spectral Analysis](https://gitlab.com/RTbecard/ezPSD/-/jobs/243441178/artifacts/file/ezPSD.Rcheck/ezPSD/doc/spectral_analysis.html)
- [Manual](https://www.gitlab.com/RTbecard/ezPSD/-/jobs/artifacts/master/file/ezPSD.Rcheck/ezPSD-manual.pdf?job=test)


### Installation

To install the package directly from github and open an example workflow, you can run the following code:

```r
require(devtools)
install_git("https://gitlab.com/RTbecard/ezPSD.git", build_vignettes = T, force = T)
require(ezPSD)

## Check vignette
vignette(package = "ezPSD")
```
Note that this downloads the latest dev version, not the latest stable version.
For the latest stable version of the code that passed the CRAN checks, you can grab it [here](https://gitlab.com/RTbecard/ezPSD/-/jobs/artifacts/master/download?job=test).

### To-do list

- Remove TuneR dependencies from `read.wav`.
  - Add `error` and `force` if wav file exceedes available memory (avoids swap memory).
  - Add `start` and `end` times (seconds) for reading wav files.
- Add `write.wav` and make compatable with TASCAM playback devices.
- Remove `signal` dependancy from  `ezButter` function (i.e. write butterworth filter from scratch).
  - I need to use SOS (not B, A) filters to avoid numeric errors.
- Add nearest `n` neightbours to `interp.sinc` function.
- Remove `signal` dependancy from `calib.frequency` calibration.
