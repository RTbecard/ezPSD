% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/propagation.R
\name{transmissionloss.ainsley2014}
\alias{transmissionloss.ainsley2014}
\alias{transmissionloss.geometric}
\title{Analytic Propagation Models}
\usage{
transmissionloss.ainsley2014(r, f = 300, z = 1, D = 10,
  atten.bottom = 0.3, H = 30, v1 = 1500, v2 = 1710)

transmissionloss.geometric(r, z = 1, D = 10, H = 30)
}
\arguments{
\item{r}{Propagation distance (meters).  This can be a vector of values.}

\item{f}{Frequency in Hertz.  This can be a vector of values to examine broadband sounds.}

\item{z}{Source depth (meters).}

\item{D}{Reciever depth.}

\item{atten.bottom}{Bottom attenuation in \emph{Np/rad}.  The default value of 0.3 represents sand/silt sediment.}

\item{H}{Water depth (meters).}

\item{v1}{Sound speed in water column (meters per second).}

\item{v2}{Sound speed in bottom sediment (meters per second).}
}
\value{
A data.frame holding the resulting Propagation Loss in dB (re 1m)
}
\description{
Here we present a short selection of analytic underwater acoustic propagation models.
}
\details{
Model Overview:
\itemize{
\item \strong{Geometric Spreading}: Only considers propagation loss as a result of spherical and cylindrical spreading and is thus range independant.  Suitable for continous sounds in environments where there are no bottom reflections.  At ranges from the source greater than the water depth, cylindrical propagation is calculated.
\item \strong{Ainsley 2014}: Range independant shallow water propataion model for continous sounds with a homogenous sound speed profile. Considers bottom absoroption and Loyd's mirror effect.  Does not consider the sahllow water waveguide cuttoff frequency.
}
}
\section{Functions}{
\itemize{
\item \code{transmissionloss.ainsley2014}: Ainsley's 2014 practical propagation algorithem.

\item \code{transmissionloss.geometric}: Geometric Spreading
}}

\examples{
###### Set model parameters
H = 30; z = 1; D = 10      # Depths of water column, source, and reciever
v1 = 1500; v2 = 1710       # Sediment Properties
f = 300                    # Frequency of interest
r = 10^seq(from = 1, to = 4, length.out = 50)    # Ranges to model (meters)

# The following example replicates figure 1 (right) from Ainsley 2014
# with an additional geometric spreading model overlayed

###### Run models
data.plot <- rbind(
 cbind(Model = "Ainsley",
       transmissionloss.ainsley2014(r = r, z = z, v1 = v1, v2 = v2, H = H,
                                    f = f, D = D)[,c(1,3)]),
 cbind(Model = "Geometric",
       transmissionloss.geometric(r = r, z = z,H = H, D = D)[,c(1,2)]))

###### Plot results
require(ggplot2)
#### Define plot breaks
breaks.maj <- c(1,2,3,5)
breaks.maj <- c(breaks.maj*10,breaks.maj*100,breaks.maj*1000)
breaks.min <- c(1:10)
breaks.min <- c(breaks.min*10,breaks.min*100,breaks.min*1000)
#### Plot
ggplot(data.plot) + 
 geom_line(aes(x = Range, y = PropagationLoss, color = Model)) + 
 scale_x_log10(breaks = breaks.maj, minor_breaks = breaks.min) + scale_y_reverse() + 
 coord_cartesian(ylim = c(70,20), xlim = c(10,5000)) + theme_bw() + 
 ggtitle("Model Comparison") + 
 ylab("Propagation Loss (dB)") + xlab("Range (km)")

}
\references{
{
Ainslie, M. A., Dahlb, P. H., de Jong, C. A., Laws, R. M. 2014.  \strong{PRACTICAL SPREADING LAWS: THE SNAKES AND LADDERS OF SHALLOW WATER ACOUSTICS}. \emph{UA2014 - 2nd International Conference and Exhibition on Underwater Acoustics}. 879-886
}
}
