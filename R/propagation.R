#' Analytic Propagation Models
#' 
#' Here we present a short selection of analytic underwater acoustic propagation models.
#'
#' Model Overview:
#' - **Geometric Spreading**: Only considers propagation loss as a result of spherical and cylindrical spreading and is thus range independant.  Suitable for continous sounds in environments where there are no bottom reflections.  At ranges from the source greater than the water depth, cylindrical propagation is calculated.
#' - **Ainsley 2014**: Range independant shallow water propataion model for continous sounds with a homogenous sound speed profile. Considers bottom absoroption and Loyd's mirror effect.  Does not consider the sahllow water waveguide cuttoff frequency.
#' 
#' 
#' @references{
#' Ainslie, M. A., Dahlb, P. H., de Jong, C. A., Laws, R. M. 2014.  **PRACTICAL SPREADING LAWS: THE SNAKES AND LADDERS OF SHALLOW WATER ACOUSTICS**. *UA2014 - 2nd International Conference and Exhibition on Underwater Acoustics*. 879-886
#' }
#'
#' @param r Propagation distance (meters).  This can be a vector of values.
#' @param f Frequency in Hertz.  This can be a vector of values to examine broadband sounds.
#' @param z Source depth (meters).
#' @param D Reciever depth.
#' @param atten.bottom Bottom attenuation in *Np/rad*.  The default value of 0.3 represents sand/silt sediment.
#' @param H Water depth (meters).
#' @param v1 Sound speed in water column (meters per second).
#' @param v2 Sound speed in bottom sediment (meters per second).
#' 
#' @examples
#' ###### Set model parameters
#'H = 30; z = 1; D = 10      # Depths of water column, source, and reciever
#'v1 = 1500; v2 = 1710       # Sediment Properties
#'f = 300                    # Frequency of interest
#'r = 10^seq(from = 1, to = 4, length.out = 50)    # Ranges to model (meters)
#'
#'# The following example replicates figure 1 (right) from Ainsley 2014
#'# with an additional geometric spreading model overlayed
#'
#'###### Run models
#'data.plot <- rbind(
#'  cbind(Model = "Ainsley",
#'        transmissionloss.ainsley2014(r = r, z = z, v1 = v1, v2 = v2, H = H,
#'                                     f = f, D = D)[,c(1,3)]),
#'  cbind(Model = "Geometric",
#'        transmissionloss.geometric(r = r, z = z,H = H, D = D)[,c(1,2)]))
#'
#'###### Plot results
#'require(ggplot2)
#'#### Define plot breaks
#'breaks.maj <- c(1,2,3,5)
#'breaks.maj <- c(breaks.maj*10,breaks.maj*100,breaks.maj*1000)
#'breaks.min <- c(1:10)
#'breaks.min <- c(breaks.min*10,breaks.min*100,breaks.min*1000)
#'#### Plot
#'ggplot(data.plot) + 
#'  geom_line(aes(x = Range, y = PropagationLoss, color = Model)) + 
#'  scale_x_log10(breaks = breaks.maj, minor_breaks = breaks.min) + scale_y_reverse() + 
#'  coord_cartesian(ylim = c(70,20), xlim = c(10,5000)) + theme_bw() + 
#'  ggtitle("Model Comparison") + 
#'  ylab("Propagation Loss (dB)") + xlab("Range (km)")
#'
#' @return A data.frame holding the resulting Propagation Loss in dB (re 1m)
#' @export
#' @md
#' @describeIn transmissionloss.ainsley2014 Ainsley's 2014 practical propagation algorithem.
transmissionloss.ainsley2014 <- function(r, f = 300, z = 1, D = 10,
                                         atten.bottom = 0.3, H = 30,
                                         v1 = 1500, v2 = 1710){
  ###### Generate constants
  r0 <- 1 # Reference distance
  psi <- acos(v1/v2)  # Critical angle of reflection on sediment (rads)
  k <- (2*pi*f)/v1 # Convert frequency to acoustic wavenumber
  kz <- k*z
  
  ##### Check model assumptions
  ## This is necessary to fit the mode-stripping region
  ## kz must be less than pi/4
  idx.temp <- which(kz > (pi/4))
  kz[idx.temp] <- (pi/2) - kz[idx.temp]
  

  ## Need to add 90 degrees to fit the cylindrical model
  ## This converts the critical grazing angle to the critical angle
  ## (or at least that what I think this does :/)
  psi <- pi/2 + psi

  ###### Propagation models
  prop.spherical <- function(r){
    return(
      40*log10(r/r0) -
      10*log10(4*(k*z*D/r0)^2))}
  prop.cylindrical <- function(r){
    return(
      10*log10(r/r0) +
      10*log10( (3*H) / (r0 * kz^2 * psi^3 ) )
    )}
  prop.modestripping <- function(r){
    return(
      25*log10(r/r0) +
        10*log10((4*kz)^(-2)*sqrt(atten.bottom^3*r0/(pi*H))))}
  
  
  results.sphere <- data.frame()
  results.cylinder <- data.frame()
  results.stripping <- data.frame()
  ## Loop ranges
  for(ranges in r){
    ## Calc close ranges and add to results
    results.sphere <- rbind(results.sphere,
                            data.frame(Range = ranges,
                                Frequency = f,
                                PropagationLoss = prop.spherical(ranges),
                                Type = "Spherical"))}
  for(ranges in r){
    results.cylinder <- rbind(results.cylinder,
                              data.frame(Range = ranges,
                                Frequency = f,
                                PropagationLoss = prop.cylindrical(ranges),
                                Type = "Cylindrical"))}
  for(ranges in r){
    results.stripping <- rbind(results.stripping,
                               data.frame(Range = ranges,
                                Frequency = f,
                                PropagationLoss = prop.modestripping(ranges),
                                Type = "Mode-stripping"))}
  
  ###### Find transition zones
  results <- data.frame()
  idx.spherical <- which(results.sphere$PropagationLoss < 
                           results.cylinder$PropagationLoss)

  idx.cylindrical <- which(results.cylinder$PropagationLoss >=
                             results.stripping$PropagationLoss)
  idx.cylindrical <- idx.cylindrical[which(!idx.cylindrical %in% idx.spherical)]

  results <- rbind(results.sphere[idx.spherical,],
                   results.cylinder[idx.cylindrical,],
                   results.stripping[-c(idx.spherical,idx.cylindrical),])
  
  return(results)
}

#' @describeIn transmissionloss.ainsley2014 Geometric Spreading
#' @export
transmissionloss.geometric <- function(r, z = 1, D = 10, H = 30){
  r0 <- 1
  ###### Propagation models
  prop.spherical <- function(r){
    s <- sqrt(r^2 + abs(diff(c(D, z)))^2)
    return(20*log10(s/r0))}
  prop.cylindrical <- function(r){
    return(10*log10(r/r0) - 10*log10(H/r0) + 20*log10(H/r0))}
  
  ###### Run Models
  results <- data.frame()
  idx.spherical <- which(r < H)
  idx.cylindrical <- which(r >= H)
  
  ## Loop ranges
  for(ranges in r[idx.spherical]){
    ## Calc close ranges and add to results
    results <- rbind(results,
                     data.frame(Range = ranges,
                                PropagationLoss = prop.spherical(ranges),
                                Type = "Spherical"))}
  for(ranges in r[idx.cylindrical]){
    results <- rbind(results,
                     data.frame(Range = ranges,
                                PropagationLoss = prop.cylindrical(ranges),
                                Type = "Cylindrical"))}
  return(results)
}